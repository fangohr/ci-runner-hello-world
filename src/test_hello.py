import os
import subprocess
import sys

import pytest


def test_stdout_is_hello_world(capfd):
    os.system("./hello")
    captured = capfd.readouterr()
    assert captured.out == "Hello World (TOKEN)\n"
    assert captured.err == ""


def test_executable_can_be_executed():
    subprocess.check_call("./hello")
